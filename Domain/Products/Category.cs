﻿namespace IWantApp.Domain.Products;

public class Category : Entity
{
    public string Name { get; private set; }
    public bool Active { get; private set; }

    public Category(string name, string createBy, string editeBy)
    {
        Name = name;
        Active = true;
        CreateBy = createBy;
        EditeBy = editeBy;
        CreatedOn = DateTime.Now;
        EditedOn = DateTime.Now;

        Validate();

    }

    private void Validate()
    {
        var contract = new Contract<Category>()
            .IsNotNullOrEmpty(Name, "Name", "O Campo Nome é obrigatório!")
            .IsGreaterOrEqualsThan(Name, 3, "Name")
            .IsNotNullOrEmpty(CreateBy, "CreateBy")
            .IsNotNullOrEmpty(EditeBy, "EditeBy");
        AddNotifications(contract);
    }

    public void EditInfo(string name, bool active, string editeBy)
    {
        Active = active;
        Name = name;
        EditeBy = editeBy;
        EditedOn = DateTime.Now;

        Validate();

    }

}
