using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Diagnostics;
using Serilog.Sinks.MSSqlServer;
using Serilog;
using IWantApp.EndPoints.Products;
using IWantApp.EndPoints.Clients;
using IWantApp.Domain.Users;

namespace IWantApp;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        //Configurando Conex�o com BD
        builder.Services.AddSqlServer<ApplicationDbContext>(
            builder.Configuration["ConnectionString:IWantDb"]);

        //configurando o Identity
        builder.Services.AddIdentity<IdentityUser, IdentityRole>(options =>
        {
            options.Password.RequireNonAlphanumeric = false;
            options.Password.RequireDigit = false;
            options.Password.RequireUppercase = false;
            options.Password.RequiredLength = 3;
            options.Password.RequireLowercase = false;
        }).AddEntityFrameworkStores<ApplicationDbContext>();
        //Configura��es de autoriza��o(Todos os endpoints necessitam de autoriza��o para serem acessados.)
        builder.Services.AddAuthorization(options =>
        {
            options.FallbackPolicy = new AuthorizationPolicyBuilder()
            .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
            .RequireAuthenticatedUser()
            .Build();
            //criando uma politica para acesso de employees(funcion�rios)
            options.AddPolicy("EmployeePolicy", p =>
                p.RequireAuthenticatedUser().RequireClaim("EmployeeCode"));
            
            options.AddPolicy("CpfPolicy", p =>
                p.RequireAuthenticatedUser().RequireClaim("CPF"));
        });
        //configura��es do JWT
        builder.Services.AddAuthentication(x =>
        {
            x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(options =>
        {
            options.TokenValidationParameters = new TokenValidationParameters()
            {
                ValidateActor = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                ClockSkew = TimeSpan.Zero,
                ValidIssuer = builder.Configuration["JwtBearerTokenSettings:Issuer"],
                ValidAudience = builder.Configuration["JwtBearerTokenSettings:Audience"],
                IssuerSigningKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(builder.Configuration["JwtBearerTokenSettings:SecretKey"]))
            };
        });

        builder.Services.AddScoped<QueryAllUsersWithClaimName>();
        builder.Services.AddScoped<QueryAllProductsSold>();
        builder.Services.AddScoped<UserCreator>();

        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        //configura Log para gravar em BD e Console 
        builder.Host.UseSerilog((context, configuration) => {
            configuration
                .WriteTo.Console()
                .WriteTo.MSSqlServer(
                    context.Configuration["ConnectionString:IWantDb"],
                        sinkOptions: new MSSqlServerSinkOptions()
                        {
                            AutoCreateSqlTable = true,
                            TableName = "LogAPI",
                        });
        });

        var app = builder.Build();

        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();
        app.UseAuthentication();
        app.UseAuthorization();

        app.MapMethods(CategoryPost.Template, CategoryPost.Methods, CategoryPost.Handle);
        app.MapMethods(CategoryGetAll.Template, CategoryGetAll.Methods, CategoryGetAll.Handle);
        app.MapMethods(CategoryPut.Template, CategoryPut.Methods, CategoryPut.Handle);
        app.MapMethods(OrderPost.Template, OrderPost.Methods, OrderPost.Handle);
        app.MapMethods(ClientPost.Template, ClientPost.Methods, ClientPost.Handle);
        app.MapMethods(ClientGet.Template, ClientGet.Methods, ClientGet.Handle);
        app.MapMethods(EmployeeGetAll.Template, EmployeeGetAll.Methods, EmployeeGetAll.Handle);
        app.MapMethods(TokenPost.Template, TokenPost.Methods, TokenPost.Handle);
        app.MapMethods(ProductPost.Template, ProductPost.Methods, ProductPost.Handle);
        app.MapMethods(ProductGetAll.Template, ProductGetAll.Methods, ProductGetAll.Handle);
        app.MapMethods(ProductGetById.Template, ProductGetById.Methods, ProductGetById.Handle);
        app.MapMethods(ProductGetShowcase.Template, ProductGetShowcase.Methods, ProductGetShowcase.Handle);
        app.MapMethods(ProductGetSold.Template, ProductGetSold.Methods, ProductGetSold.Handle);


        app.UseExceptionHandler("/error");
        app.Map("/error", (HttpContext http) => {

            var error = http.Features.Get<IExceptionHandlerFeature>()?.Error;

            if (error != null)
            {
                if (error is SqlException)
                    return Results.Problem(title: "Banco de dados desconectado!", statusCode: 500);
                else if (error is BadHttpRequestException)
                    return Results.Problem(title: "Erro de convers�o de dados para outro tipo. Observe todas as informa��es enviadas!", statusCode: 500);

            }

            return Results.Problem(title: "Ocorreu um erro!", statusCode: 500);
        });

        app.Run();
    }
}