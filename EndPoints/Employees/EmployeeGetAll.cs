﻿namespace IWantApp.EndPoints.Employees;
public class EmployeeGetAll
{
    public static string Template => "/employees";
    public static string[] Methods => new string[] { HttpMethod.Get.ToString() };
    public static Delegate Handle => Action;

    [Authorize(Policy = "EmployeePolicy")]
    public static async Task<IResult> Action(int? page, int? rows, QueryAllUsersWithClaimName query)
    {
        if (page == null || rows == null || page <= 0 || rows <= 0)
        {
            // Retornar uma resposta de erro indicando que os parâmetros são inválidos.
            return Results.BadRequest("Parâmetros de página e/ou linhas inválidos.");
        }
        var result = await query.Execute(page.Value, rows.Value);
        return Results.Ok(result);
    }
}
