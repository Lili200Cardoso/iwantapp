﻿namespace IWantApp.EndPoints.Employees;

public record ClientRequest(string Email, string Password, string Name, string Cpf);
