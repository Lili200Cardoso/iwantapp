﻿namespace IWantApp.EndPoints.Clients;

public record OrderRequest(List<Guid> ProductsIds, string DeliveryAddress);
