﻿using IWantApp.Domain.Products;

namespace IWantApp.EndPoints.Products;

public class ProductGetById
{
    public static string Template => "/products/{id}";
    public static string[] Methods => new string[] { HttpMethod.Get.ToString() };
    public static Delegate Handle => Action;

    [Authorize(Policy = "EmployeePolicy")]
    public static async Task<IResult> Action([FromRoute] Guid id,ApplicationDbContext context)
    {
        var product = context.Products.Include(p => p.Category).FirstOrDefault(p => p.Id == id);

        if (product == null) 
            return Results.NotFound($"Product with ID {id} not found.");

        var result = new ProductResponse(product.Id, product.Name, product.Category.Name, product.Description, product.HasStock,product.Price, product.Active);
        return Results.Ok(result);
    }
}
