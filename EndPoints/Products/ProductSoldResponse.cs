﻿namespace IWantApp.EndPoints.Products;

public record ProductSoldResponse(Guid id, string Name, int amount);

